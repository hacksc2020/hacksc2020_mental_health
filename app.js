var express = require('express');
var app = express();
var http=require('http');
var https= require('https');
var url= require('url');
var request = require('request');
const cors = require('cors');
//const port = 3000;
const port = process.env.PORT || 3000;

//__dirname = 'dist/weather-app';
//app.use(express.static(__dirname));

app.listen(port);

//customError = JSON.stringify(customError);

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

app.get('/youtube_urls', cors(), (req, res, next) => {
    console.log("query is: " + req.query.searchTerm);
    //var q = "Happy";
    let youtubeVideoDataAPIKey = "AIzaSyDYfSeJ-3Ay4L3chuuNfiI1vd11ex_8yK4";
    let searchTerm = req.query.searchTerm;
    let youtubeSearchURL = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" + searchTerm + "&type=video&maxResults=30&key=" + youtubeVideoDataAPIKey;

    request(youtubeSearchURL, function (error1, response1, body1) {
        var result = [];
        //console.log(response1);
        if (!error1 && response1.statusCode == 200) {
            let resBody = JSON.parse(response1.body);
            //console.log(resBody);
            //console.log(resBody.items)
            for(let videoID of resBody.items) {
                //console.log(videoID.id.videoId);
                //var videoURL = "https://www.youtube.com/watch?v=" + videoID.id.videoId;
                result.push(videoID.id.videoId);
            }
            //console.log(result);
            shuffle(result);
            //console.log(result);
            return res.json({ results: result.slice(0, 10)});
        } else {
            console.log("Error Getting Youtube URLs")
        }
    });
});

app.get('/imageSearch_urls', cors(), (req, res, next) => {
    console.log("query is: " + req.query.searchTerm);
    //var q = "Happy";
    let googleImageSearchAPIKey = "AIzaSyCWrIJ3IEFdEQ0vJImqSv90n-oYmgz8ivc";
    let imageSearchTerm = req.query.searchTerm;

    ImageSearchURL = "https://www.googleapis.com/customsearch/v1?&searchType=image" + "&key=" + googleImageSearchAPIKey + "&cx=013753080896795366332:djg4ipbmyuv&q=" + imageSearchTerm;

    request(ImageSearchURL, function (error1, response1, body1) {
        var result = [];
        //console.log(response1);
        if (!error1 && response1.statusCode == 200) {
            let resBody = JSON.parse(response1.body);
            //console.log(resBody);
            //console.log(resBody.items);

            for (let item of resBody.items) {
                result.push(item.link);
            }
            console.log(result);
            return res.json({ results: result});
        } else {
            console.log("Error Getting ImageSearch URLs");
            return null;
        }
    });
});

